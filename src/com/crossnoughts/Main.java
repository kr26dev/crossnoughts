package com.crossnoughts;

import com.crossnoughts.controller.Controller;

public class Main {

    public static void main(String[] args) {
        // передача управления контроллеру
        // через конструктор контроллера автоматически начнется новая игра
        // реализуем паттерн синглтон, более одного контроллера создавать не нужно
        Controller.getInstance();
    }
}
