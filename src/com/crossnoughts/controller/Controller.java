package com.crossnoughts.controller;

import com.crossnoughts.model.Field;
import com.crossnoughts.view.View;

/**
 *  Контроллер (по модели MVC), игровой движок.
 */
public class Controller {

    private static Controller instance;
    private Field playingField;

    private Controller() {
        // создаем игровое поле по паттерну синглтон
        this.playingField = Field.getInstance();

        startGame();
    }

    public static Controller getInstance() {
        if(instance == null){
            instance = new Controller();
        }
        return instance;
    }

    private void startGame() {
        // вывод на экран новой игры
        View.newGame();

        // подготовка игры
        prepareGame();

        while (!playingField.playingFieldFinished()) {

            // ход машины
            View.printPcMoves();
            makeMove();
            View.printPlayingField(playingField);

            if (View.requestDecision()) {
                // ход игрока
                int[] playerMove = View.requestMove();
                playingField.setNought(playerMove[0]-1, playerMove[1]-1);
                View.printPlayingField(playingField);
            } else {
                View.printExitGame();
                return;
            }

            int winner = whoWins();
            if (winner != -1) {
                View.printWhoWins(winner);
                return;
            }

            if (playingField.playingFieldFinished()) {
                View.printDeadHeat(); // ничья
                return;
            }
        }
    }

    private void prepareGame() {
        // первоначальное заполнение игрового поля
        playingField.fillEmptyValues();

        View.printPlayingField(playingField);
    }

    private int whoWins() {
        // проверяем кто победил

        int rowNoughts = 0;
        int rowCross = 0;
        int colNoughts = 0;
        int colCross = 0;

        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {

                // считаем кол-во нолей по строкам
                if (playingField.getPlayingField()[row][col] == 0) {
                    rowNoughts++;
                }

                if (playingField.getPlayingField()[row][col] == 1) {
                    rowCross++;
                }

                // считаем кол-во нолей по столбцам
                if (playingField.getPlayingField()[col][row] == 0) {
                    colNoughts++;
                }

                if (playingField.getPlayingField()[col][row] == 1) {
                    colCross++;
                }

                if (rowNoughts == 3 || colNoughts == 3) {
                    return 0;
                }

                if (rowCross == 3 || colCross == 3) {
                    return 1;
                }
            }
            rowNoughts = 0;
            rowCross = 0;
            colNoughts = 0;
            colCross = 0;
        }

        // проверка диагоналей
        // правая диагональ
        if (
                playingField.getPlayingField()[0][2] == 0
                        && playingField.getPlayingField()[1][1] == 0
                        && playingField.getPlayingField()[2][0] ==0
                ) {
            return 0;
        }

        if (
                playingField.getPlayingField()[0][2] == 1
                        && playingField.getPlayingField()[1][1] == 1
                        && playingField.getPlayingField()[2][0] ==1
                ) {
            return 1;
        }

        // левая диагональ
        if (
                playingField.getPlayingField()[0][0] == 0
                        && playingField.getPlayingField()[1][1] == 0
                        && playingField.getPlayingField()[2][2] ==0
                ) {
            return 0;
        }

        if (
                playingField.getPlayingField()[0][0] == 1
                        && playingField.getPlayingField()[1][1] == 1
                        && playingField.getPlayingField()[2][2] ==1
                ) {
            return 1;
        }

        return -1;
    }

    private void makeMove() {
        int rightDiagSum = -3; // правая диагональ как слэш - /
        int leftDiagSum = -3; // левая диагональ как обратный слэш - \
        int rowSum = 0;
        int colSum = 0;
        int resultRow = -1;
        int resultCol = -1;
        int resultDiag = -1; // 1 - правая диагональ, 2 - левая диагональ

        for (int row=0; row<3; row++) {
            for (int col=0; col<3; col++) {

                // считаем кол-во нолей по строкам
                if (playingField.getPlayingField()[row][col] == 0) {
                    rowSum++;
                }

                if (playingField.getPlayingField()[row][col] == 1) {
                    rowSum = -3;
                }

                // считаем кол-во нолей по столбцам
                if (playingField.getPlayingField()[col][row] == 0) {
                    colSum++;
                }

                if (playingField.getPlayingField()[col][row] == 1) {
                    colSum = -3;
                }

                // если в строке больше 1 ноля, то будем ходить в этой строке
                if (rowSum == 2) {
                    resultRow = row;
                }

                // если в столбце больше 1 ноля, то будем ходить в этом столбце
                if (colSum == 2) {
                    resultCol = col;
                }
            }
            rowSum = 0;
            colSum = 0;

            // проверка диагоналей
            // правая диагональ
            rightDiagSum = playingField.getPlayingField()[0][2]
                    + playingField.getPlayingField()[1][1]
                    + playingField.getPlayingField()[2][0];

            if (rightDiagSum==-1) {
                resultDiag = 1;
            }

            // левая диагональ
            leftDiagSum = playingField.getPlayingField()[0][0]
                    + playingField.getPlayingField()[1][1]
                    + playingField.getPlayingField()[2][2];

            if (leftDiagSum==-1) {
                resultDiag = 2;
            }
        }

        // делаем ход (оборонительная тактика)
        if ((resultRow + resultCol + resultDiag) != -3) {
            for (int i = 0; i < 3; i++) {

                // ход по горизонтали
                if (resultRow > -1) {
                    if (playingField.getPlayingField()[resultRow][i] == -1) { // если ячейка свободная
                        playingField.setCross(resultRow, i);
                        return;
                    }
                }

                // ход по вертикали
                if (resultCol > -1) {
                    if (playingField.getPlayingField()[i][resultCol] == -1) { // если ячейка свободная
                        playingField.setCross(i, resultCol);
                        return;
                    }
                }
            }

            // ход по диагонали
            if (resultDiag == 1) {
                if (playingField.getPlayingField()[0][2] == -1) {
                    playingField.setCross(0, 2);
                    return;
                }
                if (playingField.getPlayingField()[1][1] == -1) {
                    playingField.setCross(1, 1);
                    return;
                }

                if (playingField.getPlayingField()[2][0] == -1) {
                    playingField.setCross(2, 0);
                    return;
                }
            }

            if (resultDiag == 2) {
                if (playingField.getPlayingField()[0][0] == -1) {
                    playingField.setCross(0, 0);
                    return;
                }
                if (playingField.getPlayingField()[1][1] == -1) {
                    playingField.setCross(1, 1);
                    return;
                }

                if (playingField.getPlayingField()[2][2] == -1) {
                    playingField.setCross(2, 2);
                    return;
                }
            }
        } else { // активная тактика (наступление)
            // ставим крестики сначала в хорошие места

            // центр поля
            if (playingField.getPlayingField()[1][1] == -1) {
                playingField.setCross(1, 1);
                return;
            }

            // углы
            if (playingField.getPlayingField()[0][0] == -1) {
                playingField.setCross(0, 0);
                return;
            }

            if (playingField.getPlayingField()[0][2] == -1) {
                playingField.setCross(0,2);
                return;
            }

            if (playingField.getPlayingField()[2][0] == -1) {
                playingField.setCross(2, 0);
                return;
            }

            if (playingField.getPlayingField()[2][2] == -1) {
                playingField.setCross(2, 2);
                return;
            }

            // иначе ставим крестик в любую ячейку

            for (int r = 0; r<3; r++) {
                for (int c = 0; c<3; c++) {
                    if (playingField.getPlayingField()[r][c] == -1) {
                        playingField.setCross(r, c);
                        return;
                    }
                }
            }
        }
    }
}














