package com.crossnoughts.model;

/**
 * Игровое поле. Модель (по паттерну MVC).
 */
public class Field {
    private int[][] playingField;
    private static Field instance;

    private Field() {
        this.playingField = new int[3][3];
    }

    public static Field getInstance(){
        if (instance==null) instance = new Field(); // создадим один единственный экземпляр
        return instance;
    }

    public int[][] getPlayingField() {
        return playingField;
    }

    public void setCross(int x, int y) {
        this.playingField[x][y] = 1;
    }

    public void setNought(int x, int y) {
        this.playingField[x][y] = 0;
    }

    public void fillEmptyValues() {
        for (int row=0;row<3;row++) {
            for (int col=0;col<3;col++) {
                this.playingField[row][col] = -1;
            }
        }
    }

    public boolean cellIsBusy(int x, int y) {
        if (this.playingField[x][y] != -1) {
            return true;
        } else
            return false;
    }

    public boolean playingFieldFinished() {
        // проверка не закончилось ли поле
        for (int row=0; row<3; row++) {
            for (int col=0; col<3; col++) {
                if (this.playingField[row][col] == -1) {
                    return false;
                }
            }
        }
        return true;
    }
}
