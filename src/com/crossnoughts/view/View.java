package com.crossnoughts.view;

import com.crossnoughts.model.Field;

import java.util.Scanner;

/**
 * Представление (View) по модели MVC.
 * Отображение данных для пользователя.
 */
public class View {

    public static void newGame() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Приветствуем Вас в новой игре Крестики-нолики!");
        System.out.println("Вы играете ноликами. Первым ход делает компьютер.");
    }

    public static void printPlayingField(Field playingField) {
        // заполним номера столбцов
        System.out.print("#|");
        for (int i=1;i<=3;i++) {
            System.out.print(Integer.toString(i) + "|");
        }

        // заполним ячейки игрового поля
        for (int row=0;row<3;row++) {
            System.out.print("\n");
            System.out.print(row + 1 + "|");

            for (int col=0;col<3;col++) {
                if (playingField.getPlayingField()[row][col]==-1) {
                    System.out.print(" |");
                } else {
                    System.out.print(playingField.getPlayingField()[row][col] + "|");
                }
            }
        }
    }

    public static boolean requestDecision() {
        boolean decision = false; // решение игрока
        boolean answered = false; // дал ответ игрок или нет
        String answer;

        do {
            // повторять вопрос, пока пользователь не ответит да или нет
            Scanner scanner = new Scanner(System.in);
            System.out.println("\nБудете делать ход? Y = да, N = нет.");
            answer = scanner.next();

            if (answer.equals("Y") || answer.equals("y")) {
                decision = true;
                answered = true;
            }

            if (answer.equals("N") || answer.equals("n")) {
                decision = false;
                answered = true;
            }
        } while (!answered);

        return decision;
    }

    public static int[] requestMove() {
        boolean answeredX = false; // ввел координату X игрок или нет
        boolean answeredY = false; // ввел координату Y игрок или нет
        int answerX, answerY;
        int[] cell = new int[2];

        System.out.println("\nВаш ход. Введите координаты ячейки (x - строка, y - столбец)");
        do {
            do {
                // повторять вопрос, пока пользователь не введет правильное число строки
                Scanner scanner = new Scanner(System.in);
                System.out.println("\nВведите номер строки (от 1 до 3):");
                answerX = Byte.valueOf(scanner.next());

                if (answerX >= 0 && answerX <= 3) {
                    cell[0] = answerX;
                    answeredX = true;
                }
            } while (!answeredX);

            do {
                // повторять вопрос, пока пользователь не введет правильное число столбца
                Scanner scanner = new Scanner(System.in);
                System.out.println("\nВведите номер стоолбца (от 1 до 3):");
                answerY = Byte.valueOf(scanner.next());

                if (answerY >= 0 && answerY <= 3) {
                    cell[1] = answerY;
                    answeredY = true;
                }
            } while (!answeredY);
        } while (Field.getInstance().cellIsBusy(answerX - 1, answerY - 1)); // не занята ли ячейка
        return cell;
    }

    public static void printPcMoves() {
        System.out.println("\nХодит компьютер...");
    }

    public static void printWhoWins(int player) {
        switch (player) {
            case 0: System.out.println("\nПоздравляем! Вы победили! Игра окончена.");
            case 1: System.out.println("\nПоздравляем! Вы победили! Игра окончена.");
        }
    }

    public static void printDeadHeat() {
        System.out.println("\nНичья! Игра окончена.");
    }

    public static void printExitGame() {
        System.out.println("\nВы вышли из игры. Игра окончена.");
    }
}
